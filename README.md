# android-10-settings

#### 介绍
将`AOSP android-10.0.0_r40`的`Settings`应用转换成`Gradle`项目，在编译和签名后，能安装和运行在android-10.0.0_r40编译的x86_64模拟器上。

转换教程详见【[导入AOSP Settings到Android Studio](https://gitee.com/papermosquito/android-10-settings/wikis/)】。

#### 编译环境
Android Studio Bumblebee | 2021.1.1 Patch 2

必须使用`android-10.0.0_r40`的SDK编译应用，否则存在资源找不到的问题。

android-29-aosp-r40-platforms 下载链接：https://pan.baidu.com/s/1qVz0NLyuQ0BUucupriNYWA?pwd=wwol ，提取码：wwol。

使用方法：
1. 备份`AndroidSDK\platforms\android-29`。
2. 解压下载的SDK到`AndroidSDK\platforms`。
3. 重启AS。

#### 运行环境
可自行编译`android-10.0.0_r40`镜像，也可以下载下面的镜像。

android-image-10_r40_x64 下载链接：https://pan.baidu.com/s/1V2L-e9YR0TNjNudEC28_Zw?pwd=8r0l ，提取码：8r0l。

使用方法：
1. 如果已经下载了官方的`Android Q x86_64`镜像，请先删除它，否则会优先使用官方的镜像。
2. 解压到`AndroidSDK\system-images`然后重启Android Studio，新建模拟器，在选择镜像界面点击`x86 Images`可以看到刚刚解压的镜像，右侧界面会显示镜像为`Android Open Source Project`，证明导入成功。